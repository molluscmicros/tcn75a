/* 
TCN75A library for use with mbed SDK

Copyright 2016 Nick Kolpin (nick.kolpin@gmail.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "mbed.h"

#include "TCN75A.h"

namespace TCN75A {
    TCN75A::TCN75A(I2C *i2c, uint8_t address):
        i2c(i2c), enshutdown(false), alerthigh(false), 
        interruptmode(false), res(res9bit), qlen(queue1)
    {
        this->address = (0x48 | (address & 0x7)) << 1;
        clear();
        uint16_t val = read(regConfig);
        enshutdown = (val & 0x1) > 0;
        interruptmode = (val & 0x2) > 0;
        alerthigh = (val & 0x4) > 0;
        res = (resolution) ((val & 0x6) >> 5);
        qlen = (queue) ((val & 0x18) >> 3);
    }

    uint16_t TCN75A::read(reg r) {
        clear();
        buf[0] = r;
        //i2c->write(address, buf, 1, true);
        i2c->write(address, buf, 1);
        clear();
        if (r == regConfig) {    // Config register only 1 byte
            i2c->read(address, buf, 1);
            return (uint16_t) buf[0];
        } else {
            i2c->read(address, buf, 2);
            uint16_t value = (uint16_t) buf[0];
            value = value << 8;
            value |= (uint16_t) buf[1];
            return value;
        }
    }
    
    void TCN75A::write(reg r, uint16_t value) {
        clear();
        buf[0] = r;
        if (r == regConfig) {
            buf[1] = (uint8_t) value & 0xff;
            i2c->write(address, buf, 2);
        } else {
            buf[1] = (uint8_t) (value >> 8);
            buf[2] = (uint8_t) (value & 0xff);
            i2c->write(address, buf, 3);
        }
    }

    float TCN75A::temperature() {
        if (enshutdown) {
            // Trigger one shot then wait for reading.
            config(true, res, qlen, alerthigh, interruptmode, enshutdown);
            float dt = 1.5 * 0.03 * (0x1 << res);
            wait(dt * 2);
        }
        uint16_t val = read(regAmbientTemperature);
        bool negative = (val & 0x8000) > 0;
        val = val & 0x7fff;
        float temp = (float) val;
        temp = temp / 256.0;
        if (negative) temp  = -temp;
        return temp;
    }

    void TCN75A::configure(
        resolution res, queue qlen, bool alerthigh, bool interruptmode)
    {
        config(false, res, qlen, alerthigh, interruptmode, enshutdown);
    }

    void TCN75A::shutdown(bool enable) {
        config(false, res, qlen, alerthigh, interruptmode, enable);
    }

    void TCN75A::config(
        bool oneshot, resolution res, queue qlen, 
        bool alerthigh, bool interruptmode, bool shutdown)
    {
        this->enshutdown = shutdown;
        this->alerthigh = alerthigh;
        this->interruptmode = interruptmode;
        this->res = res;
        this->qlen = qlen;
        uint16_t value = 0x0;
        if (shutdown) value |= 0x1;
        if (interruptmode) value |= 0x2;
        if (alerthigh) value |= 0x4;
        value |= (qlen << 3);
        value |= (res << 5);
        if (oneshot) value |= 0x80;
        write(regConfig, value);
    }

    uint16_t TCN75A::limittou16(float temp) {
        bool negative = temp < 0.0;
        if (negative) temp = -temp;
        temp = temp * 256.0;
        uint16_t val = (uint16_t) temp;
        val &= 0x7f80;
        if (negative) val |= 0x8000;
        return val;
    }

    void TCN75A::setlimit(float temp) {
        write(regTemperatureLimit, limittou16(temp));
    }

    void TCN75A::sethysteresis(float temp) {
        write(regTemperatureHysteresis, limittou16(temp));
    }

    void TCN75A::clear() {
        buf[0] = 0;
        buf[1] = 0;
        buf[2] = 0;

    }
}
