/* 
TCN75A library for use with mbed SDK

Copyright 2016 Nick Kolpin (nick.kolpin@gmail.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
*/

#ifndef __MM_TCN75A_H__
#define __MM_TCN75A_H__

#include "mbed.h"

//!  TCN75A temperature sensor
namespace TCN75A {

    //!  Regisers in the TCN75A
    enum reg {
        regAmbientTemperature,
        regConfig,
        regTemperatureHysteresis,
        regTemperatureLimit
    };

    /**
     * ADC resolution enumerator
     *
     * Defines the temperature resolution: 9b = 0.5 C to 12b = 0.0625 C.
     */
    enum resolution {
        res9bit,
        res10bit,
        res11bit,
        res12bit
    };

    /** queue enumerator
     *
     * Number of consecutive measured temperatures needed to 
     * change the alert pin state: 1, 2, 4 or 6.
     */
    enum queue {
        queue1,
        queue2,
        queue4,
        queue6
    };

    /** TCN75A I2C temperature chip with alert pin
     * 
     * TCN75A can be used for continuous temperature measurements
     * that are compared to limits to set an alert pin output or
     * for one shot temeperature measurements.
     *
     * This library is hosted at https://bitbucket.org/molluscmicros/tcn75a
     * 
     * Include in your mbed project using the command:
     *
     *     mbed add bitbucket.org/molluscmicros/tcn75a
     */
    class TCN75A{
        public:
            /** TCN75A constructor.
             * 
             * @param i2c: pointer to mbed::I2C interface
             * @param address: lower three bits of I2C address, set by A0-2 pins
             */
            TCN75A(I2C *i2c, uint8_t address);
            /** Read the temperature
             * 
             * In continuous mode reads from ambient temperature register.
             * If TCN75A is shut down does a one-shot temperature measruement.
             * @returns temperature in degrees C
             */
            float temperature();
            /** Configure the TCN75A's operation.
             * @param res: resolution of temperature measurements.
             * @param qlen: number of consecutive samples that must exceed temperature limits to change alert pin state.
             * @param alerthigh: if true temperature alert drives pin high, else alert drives pin low.
             * @param interruptmode: if true temperature exceeding limits sets alert pin, which is reset by reading any register. If false alert pin is deasserted once temperature is measured below limit.
             */
            void configure(resolution res, queue qlen, bool alerthigh, bool interruptmode);
            /** Enable or disable shutdown mode
             *
             * While shut down current ~1 uA, while in continuous mode 
             * current < 200 uA.
             * 
             * @param enable: true for shutdown, false for continuous sampling
             */
            void shutdown(bool enable);
            /** Set temperature limit for alerts.
             * @param temperature: temperature in C, above which alert asserted.
             */
            void setlimit(float temperature);
            /** Set hysteresis limit for alerts.
             * @param temperature: temperature in C, below which alert de-asserted (comparison mode) or addition allert asserted (ineterrupt mode).
             */
            void sethysteresis(float temperature);
        private:
            void config(bool oneshot, resolution res, queue qlen, bool alerthigh, bool interruptmode, bool shutdown);
            void clear();
            uint16_t read(reg);
            void write(reg, uint16_t);
            uint16_t limittou16(float);
            I2C *i2c;
            uint8_t address;
            char buf[3];
            bool enshutdown, alerthigh, interruptmode;
            resolution res;
            queue qlen;
    };
}

#endif
