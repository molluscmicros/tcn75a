TCN75A 2-Wire Serial Temperature Sensor
=======================================

This library controls a TCN75A I2C temperature sensor chip using the mbed tool chain.
The TCN75A can either taking continuous measurements which get compared against temperature limits to set an alert line.
Alternatively the chip can be powered down and take individual measurements on demand, as demonstrated in the example code below.

http://ww1.microchip.com/downloads/en/DeviceDoc/21935D.pdf


To install using the mbed-cli:

````
mbed add bitbucket.org/molluscmicros/tcn75a
````

Example usage:
````
#include "mbed.h"

#include "TCN75A.h"

I2C i2c(p9, p10); // One of the I2C buses on LPC1768 mbed
TCN75A::TCN75A tempsensor(&i2c, 0x0); // A0 = A1 = A2 = LOW
float t;

int main() {
    // Power down the sensor
    tempsensor.configure(false, TCN75A::res12bit, TCN75A::queue1, false, false, true);
    while(1) {
        // Single shot temperature measurement
        t = tempsensor.temperature();
        printf("TEMP: %f\n", t);
        wait(10.0);
    }
};
````

License
-------

Copyright 2016 Nick Kolpin (nick.kolpin@gmail.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
